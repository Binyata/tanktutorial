// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TankTutorialGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TANKTUTORIAL_API ATankTutorialGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
